import React, { Component, Fragment } from 'react';

import Modif_Heure from '../Modif_Heure';
import Modif_Date from '../Modif_Date';











var mois=["January","February","March","April","May","June","July","August","September","October","November","December"];
var jours=["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];

 

class Affichage extends Component {



  
  state = {
    currentDate: new Date(), 
    editing: false, 
    edited: false 
  }

  
  componentDidMount() {
    this.timeID = setInterval(() => this.tick(), 1000);
  }

  
  componentWilUnmount() {
    clearInterval(this.timeID);
  }

  
  handleChange = (e) => {

    
    if (!e.target.value) return false;

    
    const newDate = this.state.currentDate;

    
    if (e.target.name === "mytime") {
      const heumin = e.target.value.split(":");
      newDate.setHours(heumin[0], heumin[1]);
      
    } else {
    
      const jou_moi_an = e.target.value.split("-");
      newDate.setFullYear(parseInt(jou_moi_an[0]))
      newDate.setMonth(parseInt(jou_moi_an[1]) - 1)
      newDate.setDate(parseInt(jou_moi_an[2]))
    }

  
    this.setState({ currentDate: newDate, edited: true })

  }


  

  
  resetChanges = () => {
    this.setState({ currentDate: new Date(), edited: false });
  }

  
  tick() {
    if (!this.state.edited) {
      this.setState({
        currentDate: new Date()
      });
    }
  }

  
  ajouteZero = (n) => n > 9 ? n : "0" + n;

    
    

 render() {

    const { currentDate } = this.state;

    
    const day = currentDate.getDay();
    const date = this.ajouteZero(currentDate.getDate());
    const month = currentDate.getMonth();
    const year = currentDate.getFullYear();

    const hour = this.ajouteZero(currentDate.getHours());
    const minutes = this.ajouteZero(currentDate.getMinutes());

    
    let periode = "MATIN";
    if (hour > 12 && hour < 16) {
      periode = "APRES-MIDI";
    } else if (hour >= 16) {
      periode = "SOIR";
    } else if(hour ===12){
      periode="MIDI";
    }



       
        return (


       


        <Fragment>
           <div style={{backgroundColor:'#e6e6fa',borderTop:'45px solid #e6e6fa',height:'350px',width:'800px',
                marginLeft:'auto',marginRight:'auto',marginTop:'0px',textAlign:'center'}}>

        

                <div style={{backgroundColor:'black', border:'5px solid #808080', height:'300px',
                        width:'500px',marginLeft:'auto',marginRight:'auto',marginTop:'auto',marginBottom:'auto'
                       ,borderRadius:'4px' }}>

                            <h1 style={{color:'black'}}> {jours[day]} </h1>
            
                            <h3 style={{color:'white'}}>{periode}</h3>
            
                            <h2 style={{color:'white'}}>{hour}:{minutes}</h2>
            
                            <h3 style={{color:'white'}}>{date} {mois[month].toUpperCase()} {year}</h3>
            
                </div>

            </div>

            <div title='Modifier les parametres heure et date de l&#39;horloge'style={{display:'flex',
              marginLeft:'auto', marginRight:'auto',justifyContent:'space-between',
                          width:'50%',height:'90px',backgroundColor:'black',border:'4px solid #d3d3d3'}}>
                  
                
                <div style={{marginTop:'20px',marginBottom:'20px',marginRight:'auto',marginLeft:'auto'}}>
                    <Modif_Heure changer_heure={this.handleChange} />
                </div>
                <div style={{marginTop:'20px',marginBottom:'20px',marginLeft:'auto',marginRight:'auto'}}>
                    
                  <Modif_Date  changer_Date={this.handleChange} />
                </div>

                
              </div>
                       
                
        </Fragment>
                
            
    
        )
    }

    
} 


export default Affichage;