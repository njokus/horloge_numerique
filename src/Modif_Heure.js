import React from 'react';
import App from './App'

const Modif_Heure = ({changer_heure}) => {
    return (
        <div>
               <input type="time" title='veuillez modifier l&#39;heure' name="mytime" onChange={changer_heure}/>
        </div>
    );
};

export default Modif_Heure;