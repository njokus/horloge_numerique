import React from 'react';

const Modif_Jour = () => {
    return (


        <div>
             <select name="SEMAINE" id="semaine" title='veuillez modifier le jour'>
                <option value="LUNDI">LUNDI</option>
                <option value="MARDI">MARDI</option>
                <option value="MERCREDI">MERCREDI</option>
                <option value="JEUDI">JEUDI</option>
                <option value="VENDREDI">VENDREDI</option>
                <option value="SAMEDI">SAMEDI</option>
                <option value="DIMANCHE">DIMANCHE</option>
            </select>
        </div>
    );
};

export default Modif_Jour;